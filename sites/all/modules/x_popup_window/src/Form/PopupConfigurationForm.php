<?php

namespace Drupal\x_popup_window\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures forms module settings.
 *
 * @package Drupal\x_popup_window\Form
 */
class PopupConfigurationForm extends ConfigFormBase {

  const SETTINGS_FILE = 'x_popup_window.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'x_popup_window_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_FILE];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $req = NULL) {
    $config = $this->config(self::SETTINGS_FILE);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title of a popup window'),
      '#default_value' => $config->get('title'),
    ];

    $form['window_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width of popup window'),
      '#default_value' => $config->get('window_width'),
      '#min' => $this->t('400'),
      '#step' => $this->t('2'),
    ];

    $form['window_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height of popup window'),
      '#default_value' => $config->get('window_height'),
      '#min' => $this->t('200'),
      '#step' => $this->t('2'),
    ];

    $form['title_area_bg'] = [
      '#type' => 'color',
      '#title' => $this->t('Title background color'),
      '#default_value' => $config->get('title_area_bg'),
    ];

    $form['title_area_fg'] = [
      '#type' => 'color',
      '#title' => $this->t('Title foreground color'),
      '#default_value' => $config->get('title_area_fg'),
    ];

    $form['content_bg'] = [
      '#type' => 'color',
      '#title' => $this->t('Content background color'),
      '#default_value' => $config->get('content_bg'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config(self::SETTINGS_FILE)
      ->set('title', $values['title'])
      ->set('window_width', $values['window_width'])
      ->set('window_height', $values['window_height'])
      ->set('title_area_bg', $values['title_area_bg'])
      ->set('title_area_fg', $values['title_area_fg'])
      ->set('content_bg', $values['content_bg'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}