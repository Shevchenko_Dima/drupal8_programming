(function($, Drupal, drupalSettings) {
  $(document).ready(function() {

    var drupalSettingsParams = drupalSettings['x_popup_window']['popup-window'];

    var body = document.body;

    var $x_popup_overlay = $('.popup-overlay');
    var $x_popup_draggable = $('.popup-draggable');
    var $x_popup_header = $('.popup-draggable__header');
    var $x_popup_title = $('.popup-draggable__title');
    var $x_popup_content = $('.popup-draggable__content');

    var $elemToDrag = $x_popup_draggable;

    /*
     * +------------------------+
     * | Parameters application |
     * +------------------------+
     */
    $x_popup_draggable.css('width', drupalSettingsParams['window_width']);
    $x_popup_draggable.css('height', drupalSettingsParams['window_height']);
    $x_popup_content.css('background-color', drupalSettingsParams['content_bg']);
    $x_popup_header.css('background-color', drupalSettingsParams['title_area_bg']);
    $x_popup_title.css('color', drupalSettingsParams['title_area_fg']);
    $x_popup_title.text(drupalSettingsParams['title']);

    $('.pseudo-link').on('click', function () {
      $x_popup_overlay.show();
      $x_popup_draggable.show();
    });

    /*
     * +----------------+
     * | Overlay hiding |
     * +----------------+
     */
    function hideOverlay() {
      $x_popup_overlay.hide();
      $elemToDrag.hide();
    }

    $x_popup_overlay.on('click', hideOverlay);

    /*
     * +----------------------+
     * |      Drag n drop     |
     * +----------------------+
     */
    function getCoords(elem) {
      var docEl = document.documentElement;

      var box = elem.getBoundingClientRect();

      var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
      var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

      var clientTop = docEl.clientTop || body.clientTop || 0;
      var clientLeft = docEl.clientLeft || body.clientLeft || 0;

      var top = box.top + scrollTop - clientTop;
      var left = box.left + scrollLeft - clientLeft;

      return {
        top: top,
        left: left
      };
    }

    var draggableContainer = document.getElementById('x-popup-draggable');
    var draggableHeader = document.getElementById('x-popup-header');
    var draggableClose = document.getElementById('x-popup-close');

    draggableHeader.onmousedown = function (e) {
      e.preventDefault();

      if (e.target === draggableClose) {
        hideOverlay();
        return false;
      }

      var coords = getCoords(draggableContainer);
      var shiftX = e.pageX - coords.left;
      var shiftY = e.pageY - coords.top;

      body.appendChild(draggableContainer);
      moveAt(e);

      var $limitX = $(window).width() - $x_popup_draggable.width(),
        $limitY = $(window).height() - $x_popup_draggable.height();

      function moveAt(e) {
        var posX = e.pageX - shiftX;
        var posY = e.pageY - shiftY;
        if (posX <= $limitX && posY <= $limitY && posX > 0 && posY > 0) {
          draggableContainer.style.left = posX + 'px';
          draggableContainer.style.top = posY + 'px';
        }
      }

      document.onmousemove = function (e) {
        e.preventDefault();
        moveAt(e);
      };

      draggableHeader.onmouseup = function () {
        document.onmousemove = null;
        draggableHeader.onmouseup = null;
      }
    }

  });

})(jQuery, Drupal, drupalSettings);
