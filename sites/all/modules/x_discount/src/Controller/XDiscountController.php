<?php

namespace Drupal\x_discount\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class XDiscountController.

 * @package Drupal\x_discount\Controller
 */
class XDiscountController extends ControllerBase {

  /**
   * Returns a simple popup example page.
   *
   * @return array
   *   A simple able to render array.
   */
  public function page() {
    return [
      '#theme' => 'x_discount',
    ];
  }

}
