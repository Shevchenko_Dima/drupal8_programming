<?php

namespace Drupal\x_discount\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class XDiscountAdminForm.
 *
 * @package Drupal\x_discount\Form
 */
class XDiscountAdminForm extends ConfigFormBase {

  const SETTINGS_FILE = 'x_discount.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'x_discount_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_FILE];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_FILE);

    $form['x_discount_welcome_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Welcome message'),
      '#description' => $this->t('Welcome message text'),
      '#default_value' => $config->get('welcome_description'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conf = $this->config(self::SETTINGS_FILE);

    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
      $conf->set($key, $value);
    }

    $conf->save();
  }

}
